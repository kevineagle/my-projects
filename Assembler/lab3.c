#include <stdio.h>

// Deklaracja funkcji które dołączone zostaną
// do programu dopiero na etapie linkowania kodu
extern int symbol_newtona(int n, int k);

// Deklaracja zmiennych
int n;
int k;

int main(void)
{
	// Deklaracja zmiennych
	int n;
	int k;
	int a;
	// Wywołanie funkcji Asemblerowej
	printf("Podaj liczby n i k:\n");
	scanf("%d",&n);
	scanf("%d",&k);
	a=symbol_newtona(n, k);
	
	// Wyświetlenie wyniku
	printf("Wynik: %d\n", a);
	
	// Zwrot wartości EXIT_SUCCESS na wyjściu programu
	return 0;
}
