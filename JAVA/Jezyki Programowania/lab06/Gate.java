package lab06_pop;

import java.net.*;
import java.util.Random;
import java.io.*;

import javax.swing.*;
import java.awt.event.*;
import javax.swing.GroupLayout.Alignment;
import java.awt.Font;

class Gate extends JFrame implements ActionListener, Runnable {

	private static final long serialVersionUID = 1L;
	private JTextArea textArea = new JTextArea(10, 18);
	public int a;
	static final int SERVER_PORT = 15000;
	static final int SERVER_PORT1 = 15001;
	private String nazwa = "Bramka";
	private String host;
	private Socket socket;
	private Socket socket1;
	private ObjectOutputStream output;
	private ObjectOutputStream output1;
	private ObjectInputStream input;
	public String m;

	Gate() {
		super("Bramka");
		setSize(377, 308);
		this.setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JPanel panel = new JPanel();

		Random generator = new Random();
		JButton btnNewButton = new JButton("Stworz Bramke");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				a = 1000 + (generator.nextInt(8999));
				m = "bramka zostala stworzona";
				try {
					m = "bramka zostala stworzona";
					output.writeObject(m);
					output1.writeObject(a);
					textArea.setText("< " + m + "\n" + "port: " + a + "\n");
					if (m.equals("exit")) {
						input.close();
						output.close();
						output1.close();
						socket.close();
						setVisible(false);
						dispose();
						return;
					}
				} catch (IOException e) {
				}
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		setContentPane(panel);
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		
		JLabel lblStan = new JLabel("Stan:");
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(63)
							.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(13)
							.addComponent(lblStan)
							.addGap(18)
							.addComponent(textArea, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(167, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(5)
							.addComponent(textArea, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(65)
							.addComponent(lblStan)))
					.addContainerGap(65, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		Thread t = new Thread(this);
		t.start();
		setVisible(true);
	}

	public void run() {
		try {
			host = InetAddress.getLocalHost().getHostName();
			socket = new Socket(host, SERVER_PORT);
			socket1 = new Socket(host, SERVER_PORT1);
			input = new ObjectInputStream(socket.getInputStream());
			output = new ObjectOutputStream(socket.getOutputStream());
			output1 = new ObjectOutputStream(socket1.getOutputStream());
			output.writeObject(nazwa);
			output1.writeObject(nazwa);
		} catch (IOException e) {
		}
		try {
			while (true) {
				String m = (String) input.readObject();
				String pom = "Bramka";
				textArea.setText(pom + ">>> " + m + "\n");
				if (m.equals("exit")) {
					input.close();
					output.close();
					output1.close();
					socket.close();
					setVisible(false);
					dispose();
					break;
				}
			}
		} catch (Exception e) {
		}
	}

	public static void main(String[] args) {

		new Gate();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}
}