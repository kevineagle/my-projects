package Paczka;

import java.sql.SQLException;
import javax.swing.JFrame;

public class Main {

	public static void main(String[] args) throws SQLException {

		GUI app = new GUI();
		app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		app.setVisible(true);

	}

}
