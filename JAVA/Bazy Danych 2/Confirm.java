package Paczka;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class Confirm extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	boolean logon = false;

	String url = "jdbc:mysql://localhost:3306";
	String dbuser = "root";
	String dbpassword = "";
	static String auser = null;
	private JLabel ls;
	private JButton back2;
	private JButton add;
	private JButton remove;
	private JButton res;
	private DBCon con;
	private ResultSet result;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBox;
	GUI window;
	static Login windowe;
	User asda;
	static Confirm frame3;

	public void setWindowse(User use, Login asd, GUI cos, String user) {
		asda = use;
		windowe = asd;
		window = cos;
		auser = user;

	}

	ArrayList<Reservation> resv;

	private static Object[][] resvTable;
	public static int countmovies;
	private final static Object[] columns = { "Id", "Movie ID", "Confirm?" };
	DefaultTableModel model = new DefaultTableModel();
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame3 = new Confirm(auser);
					frame3.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @param auser2
	 * 
	 * @throws SQLException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Confirm(String auser2) throws SQLException {
		super("Confirm Reservation");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING).addGap(0, 774, Short.MAX_VALUE));
		gl_contentPane.setVerticalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING).addGap(0, 551, Short.MAX_VALUE));
		contentPane.setLayout(gl_contentPane);

		resv = new ArrayList<Reservation>();
		con = new DBCon(url, dbuser, dbpassword);

		ls = new JLabel("Choose Payment:");
		ls.setFont(new Font("Tahoma", Font.BOLD, 14));
		ls.setLocation(5, 405);
		ls.setSize(160, 25);
		getContentPane().add(ls);

		result = con.getReservation();
		while (result.next()) {
			resv.add(new Reservation(result.getString("ID"), result.getString("user_ID"), result.getString("movie_ID"),
					result.getString("confirm")));
		}
		resvTable = new String[resv.size()][3];
		model.setDataVector(resvTable, columns);
		countmovies = resv.size();
		for (int i = 0; i < resv.size(); i++) {
			if (Integer.toString(con.usid(auser2)).equals(resv.get(i).Your_ID)) {
				resvTable[i][0] = resv.get(i).Id;
				resvTable[i][1] = resv.get(i).Movie_ID;
				resvTable[i][2] = resv.get(i).Confirm;
			}
		}

		table = new JTable(resvTable, columns);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setLocation(5, 5);
		scrollPane.setSize(575, 400);
		getContentPane().add(scrollPane);
		table.setFillsViewportHeight(true);

		back2 = new JButton("Back");
		back2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				dispose();
				if (asda != null) {
					asda.setVisible(true);
					asda.setWindowse(windowe, window, auser);

				}
			}
		});
		back2.setFont(new Font("Tahoma", Font.BOLD, 14));
		back2.setSize(150, 25);
		back2.setLocation(625, 35);
		getContentPane().add(back2);
		back2.setVisible(true);

		add = new JButton("Add Movie");
		add.setFont(new Font("Tahoma", Font.BOLD, 14));
		add.setSize(123, 25);
		add.setLocation(455, 495);

		remove = new JButton("Remove Movie");
		remove.setFont(new Font("Tahoma", Font.BOLD, 14));
		remove.setSize(123, 25);
		remove.setLocation(455, 525);

		res = new JButton("Confirm reservation");
		res.setFont(new Font("Tahoma", Font.BOLD, 14));
		res.setSize(180, 25);
		res.setLocation(5, 495);

		ResHandler resHandler = new ResHandler();
		res.addActionListener(resHandler);
		getContentPane().add(res);
		res.setVisible(true);

		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "card", "cash" }));
		comboBox.setSize(60, 25);
		comboBox.setLocation(145, 405);
		getContentPane().add(comboBox);
		comboBox.setVisible(true);

	}

	private class ResHandler implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			@SuppressWarnings("unused")
			String payment;
			payment = (String) comboBox.getSelectedItem();
			String id = (String) table.getValueAt(table.getSelectedRow(), 0);
			String cash = (String) table.getValueAt(table.getSelectedRow(), 1);

			int a;
			a = con.makeConfirmReservation(cash, id, payment);
			if (a == 1) {
				JOptionPane.showMessageDialog(null, "Reservation Confirmed");
			} else {
				JOptionPane.showMessageDialog(null, "Can't Confirm");
			}

		}
	}

}