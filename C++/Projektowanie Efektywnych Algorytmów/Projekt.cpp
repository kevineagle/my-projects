// Projekt 3.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "Komiwojazer.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <iomanip> 
#include <string>
#include <windows.h> 

using namespace std;
int operacja = 0;
int decyzja = 0;
Komiwojazer *komiwojazer = new Komiwojazer();
string plik;
//-----------------------------CZAS----------------------------------------

long long int read_QPC()
{

	LARGE_INTEGER count;
	QueryPerformanceCounter(&count);
	return((long long int)count.QuadPart);

}
//------------------------------------------------------------------------- 
void zakladka()
{

	cout << "                         =============================================================" << endl;
	cout << "                         |                           PROJEKT                          |" << endl;
	cout << "                         |            Projektowanie Efektywnych Algorytmów            |" << endl;
	cout << "                         |                   Autor: Kevin Sienkiewicz                 |" << endl;
	cout << "                         |                                                            |" << endl;
	cout << "                         =============================================================" << endl << endl << endl << endl;


}
void opcje()
{
	cout << "==== MENU GLOWNE ===" << endl;
	cout << "Dostepne opcje:" << endl;
	cout << "1. Problem Komiwojazera" << endl;
	cout << "2. Wyjscie z programu" << endl;
	cout << endl << "Ktora opcje wybierasz?" << endl;
	cout << endl << "Twoj wybor: ";
}
void operacjekomiwojazera()
{
	cout << "Ktora operacje chcesz wykonac?" << endl;
	cout << "1. Wczytaj  z pliku .txt" << endl;
	cout << "2. Wygeneruj  losowo" << endl;
	cout << "3. Wyswietl" << endl;
	cout << "4. Algorytm: Programowanie Dynamiczne" << endl;
	cout << "5. Algorytm: Tabu Search" << endl;
	cout << "6. Algorytm: Genetyczny" << endl;
	cout << "7. Algorytm: Mrowkowy" << endl;
	cout << "8. Algorytm: Przeglad Zupelny" << endl;
	cout << "9. Powrot do menu" << endl;
	cout << endl << "Twoj wybor: ";
}
void komiwojazerr()
{
	long long int frequency, start, elapsed, wynik = 0;
	QueryPerformanceFrequency((LARGE_INTEGER *)&frequency);
	operacjekomiwojazera();
	cin >> operacja;
	int tabu;
	switch (operacja)
	{
	case 1:
	{
		//Wczytywanie pliku .txt

		cout << "Podaj nazwe pliku: ";
		cin >> plik;
		komiwojazer->wczytywanie(plik);

		komiwojazerr();

	}
	break;
	case 2:
	{
		//Generowanie
		int ilosc;
		cout << "Podaj liczbe wierzcholkow: ";
		cin >> ilosc;
		komiwojazer->wygeneruj(ilosc);
		komiwojazerr();
	}
	break;
	case 3:
	{
		//Wyswietlanie
		komiwojazer->wyswietl();
		komiwojazerr();
	}
	break;
	case 4:
	{
		// Algorytm Programowania Dynamicznego

		start = read_QPC();
		komiwojazer->algorytmProgramowanieDynamiczne();
		elapsed = read_QPC() - start;
		

		cout << endl << "Algorytm zostal wykonany" << endl << "Czas wykonania algorytmu" << " [ms] = " << setprecision(0) << (1000.0 * elapsed) / frequency << endl << endl;

		komiwojazerr();

	}
	break;
	case 5:
	{
		// Algorytm Tabu Search
		cout << "Podaj wielkosc tabu" << endl;
		cin >> tabu;

		start = read_QPC();
		komiwojazer->algorytmTabuSearch(tabu);
		elapsed = read_QPC() - start;


		cout << endl << "Algorytm zostal wykonany" << endl << "Czas wykonania algorytmu" << " [ms] = " << setprecision(0) << (1000.0 * elapsed) / frequency << endl << endl;

		komiwojazerr();
	
	}
	break;
	case 6:
	{
		// Algorytm Genetyczny
		int populacja, iteracje;
		cout << "Populacja: ";
		cin >> populacja;
		cout << "Iteracje algorytmu: ";
		cin >> iteracje;

		start = read_QPC();
		komiwojazer->algorytmGenetyczny(populacja, iteracje);
		elapsed = read_QPC() - start;


		cout << endl << "Algorytm zostal wykonany" << endl << "Czas wykonania algorytmu" << " [ms] = " << setprecision(0) << (1000.0 * elapsed) / frequency << endl << endl;

		komiwojazerr();

	}
	break;
	case 7:
	{
		// Algorytm Mrówkowy
		int mrowka;
		cout << "Podaj liczbe mrowek" << endl;
		cin >> mrowka;
		start = read_QPC();
		komiwojazer->algorytmMrówkowy(mrowka);
		elapsed = read_QPC() - start;


		cout << endl << "Algorytm zostal wykonany" << endl << "Czas wykonania algorytmu" << " [ms] = " << setprecision(0) << (1000.0 * elapsed) / frequency << endl << endl;

		komiwojazerr();

	}
	break;
	case 8:
	{
		// Algorytm Przeglad zupelny

		start = read_QPC();
		komiwojazer->przegladZupelny();
		elapsed = read_QPC() - start;


		cout << endl << "Algorytm zostal wykonany" << endl << "Czas wykonania algorytmu" << " [ms] = " << setprecision(0) << (1000.0 * elapsed) / frequency << endl << endl;

		komiwojazerr();

	}
	break;
	case 9:
	{
		// Powrót do menu głównego
		system("CLS");
		zakladka();

	}
	break;
	default:
	{
		//Bledny wybór opcji
		cout << endl << "Brak takiej operacji" << endl;
		cout << endl << "Wybierz ponownie" << endl;
		komiwojazerr();
	}
	break;
	}
}
void menu()
{

	opcje();
	cin >> operacja;
	switch (operacja)
	{
	case 1:
	{
		cout << "              PROBLEM KOMIWOJAŻERA              " << endl;
		komiwojazerr();
		menu();

	}
	break;
	case 2:
	{
		//Wyjscie
		exit(0);
	}
	break;
	default:
	{
		cout << endl << "Brak takiej operacji" << endl;
		menu();
	}
	break;
	}
}
int main()
{
	int operacja = 0;
	zakladka();
	menu();
	system("PAUSE");
	return 0;
}