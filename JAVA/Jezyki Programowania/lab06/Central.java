
package lab06_pop;

import java.net.*;
import java.io.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

class Central extends JFrame implements ActionListener, Runnable {

	private static final long serialVersionUID = 1L;

	private JLabel e_text = new JLabel("Stan:");
	private JComboBox<Object> menuKlient = new JComboBox<Object>();
	private JTextField message = new JTextField(20);
	private JTextArea textArea = new JTextArea(15, 18);
	private JScrollPane scroll = new JScrollPane(textArea, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

	static final int SERVER_PORT1 = 15000;
	static final int SERVER_PORT = 15001;
	private String nazwa = "Centrala";
	private String host;
	private ServerSocket serwer;
	private Socket socket1;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	public String m;
	public int sad;

	private final JTextArea textArea_1 = new JTextArea();
	private final JButton btnUsunBramke = new JButton("Usun Bramke");

	Central() {
		super("Centrala");
		btnUsunBramke.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				m = "bramka zostala usunieta";
				try {
					if(sad!=0){
					output.writeObject(m);
					textArea.setText("< " + m + "\n" + "port: " + sad + "\n");
					sad=0;
						
					}
					else
					{
						output.writeObject("Nie mozna usunac juz zadnej bramki!!");
					}
				} catch (IOException e1) {
				}
			}
		});
		setSize(458, 351);
		this.setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JPanel panel = new JPanel();

		message.addActionListener(this);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setEditable(false);
		setContentPane(panel);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel
				.createSequentialGroup().addGap(70).addComponent(e_text).addGap(5)
				.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
								.addComponent(textArea_1, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
								.addGap(18).addComponent(btnUsunBramke))
						.addComponent(scroll, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE))
				.addContainerGap(156, Short.MAX_VALUE)));
		gl_panel.setVerticalGroup(gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup().addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup().addGap(91).addComponent(e_text))
						.addGroup(gl_panel.createSequentialGroup().addGap(5).addComponent(scroll,
								GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGap(18)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(textArea_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(btnUsunBramke))
						.addContainerGap(80, Short.MAX_VALUE)));
		panel.setLayout(gl_panel);
		Thread t = new Thread(this); // Uruchomienie dodatkowego watka
		t.start();

		Thread d = new Thread(this);
		d.start();
		setVisible(true);
	}

	synchronized public void wypiszOdebrane3(Watek3 watek3, int s) {
		sad = s;
		String sas = Integer.toString(s);
		String pom = textArea.getText();
		textArea.setText("Port: " + " > " + s + "\n" + pom);
		textArea_1.setText(sas);
	}

	public void actionPerformed(ActionEvent evt) {
		String m;
		Object src = evt.getSource();
		if (src == message) {
			Watek k = (Watek) menuKlient.getSelectedItem();
			if (k != null) {
				try {
					m = message.getText();
					k.getOutput().writeObject(m);

					if (m.equals("exit")) {

					}
				} catch (IOException e) {
				}
			}
		}
		repaint();
	}

	public void run() {
		Socket s;
		Watek3 k;

		try {
			host = InetAddress.getLocalHost().getHostName();
			serwer = new ServerSocket(SERVER_PORT);

			socket1 = new Socket(host, SERVER_PORT1);
			input = new ObjectInputStream(socket1.getInputStream());
			output = new ObjectOutputStream(socket1.getOutputStream());
			output.writeObject(nazwa);
		} catch (IOException e) {

		}

		while (true) {
			try {
				s = serwer.accept();
				if (s != null) {
					k = new Watek3(this, s);
				}
			} catch (IOException e) {
			}
		}
	}

	public static void main(String[] args) {
		new Central();
	}

}

class Watek3 implements Runnable {
	private Socket socket;
	private ObjectOutputStream output;
	private ObjectInputStream input;

	private String nazwa;
	private Central okno;

	Watek3(Central mon2, Socket s) throws IOException {
		okno = mon2;
		socket = s;
		Thread t = new Thread(this); // Utworzenie dodatkowego watka
		t.start(); // do obslugi komunikacji sieciowej
	}

	public String getNazwa() {
		return nazwa;
	}

	public ObjectOutputStream getOutput() {
		return output;
	}

	public String toString() {
		return nazwa;
	}

	public void run() {
		int m;
		String pom;
		try {
			output = new ObjectOutputStream(socket.getOutputStream());
			input = new ObjectInputStream(socket.getInputStream());
			nazwa = (String) input.readObject();

			while (true) {
				m = (int) input.readObject();
				okno.wypiszOdebrane3(this, m);

			}
		} catch (Exception e) {
		}
	}
}
