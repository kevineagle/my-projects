package Paczka;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBCon {

	Statement stt;
	ResultSet res;
	ResultSet res2;

	public DBCon(String url, String dbuser, String dbpassword) {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection(url, dbuser, dbpassword);

			stt = con.createStatement();

			stt.execute("USE base");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void getUser(String username, String password, String email) {
		try {
			stt.executeUpdate("INSERT INTO users " + "VALUES (NULL, '" + password + "', '" + username + "', 'USER', '"
					+ email + "')");

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	void addMovie(String title, String director, String date, String genre, String type, String price) {
		try {
			stt.executeUpdate("INSERT INTO movies " + "VALUES (NULL, '" + title + "', '" + director + "', '" + date
					+ "', '" + type + "', '" + genre + "', 'available', '" + price + "')");

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	ResultSet getMovies() {
		try {
			res = stt.executeQuery("SELECT * FROM movies ORDER BY id");
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	ResultSet getReservation() {
		try {
			res = stt.executeQuery("SELECT * FROM res ORDER BY id");
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	ResultSet searchMoviesTitle(String condition) {
		String prepStt;
		if (condition == null) {
			prepStt = "SELECT * FROM movies";
		} else
			prepStt = "SELECT * FROM movies WHERE UPPER(title) LIKE UPPER('%" + condition + "%')";

		try {
			res = stt.executeQuery(prepStt);
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	ResultSet searchMoviesDirector(String condition) {
		String prepStt = "SELECT * FROM movies WHERE director LIKE '%" + condition + "%'";

		try {
			res = stt.executeQuery(prepStt);
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	boolean authenticateUser(String username, String password) {
		try {
			res = stt.executeQuery("SELECT name, password FROM users WHERE name = '" + username + "'");
			if (res.next()) {
				String psw = res.getString("password");
				if (psw.equals(password)) {
					return true;
				} else
					return false;
			} else
				return false;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	boolean userType(String username) {
		try {
			res = stt.executeQuery("SELECT type FROM users WHERE name = '" + username + "'");
			if (res.next()) {
				String type = res.getString("type");
				if (type.equals("ADMIN")) {
					return true;
				} else
					return false;
			} else
				return false;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	int usid(String user) {
		int a = 0;
		try {
			res = stt.executeQuery("SELECT login FROM users WHERE name = '" + user + "'");
			while (res.next()) {
				int usid = res.getInt(1);
				a = usid;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// System.out.println(a);
		return a;

	}

	int movid(String movid) {
		int aa = 0;
		try {
			res = stt.executeQuery("SELECT ID FROM movies WHERE title = '" + movid + "'");
			while (res.next()) {
				int usida = res.getInt(1);
				aa = usida;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// System.out.println(aa);
		return aa;

	}

	void changeStatus(String movie) {
		try {
			stt.executeUpdate(
					"UPDATE `movies` SET `status` = 'reservated' WHERE `movies`.`ID` = '" + movid(movie) + "' ");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	void changeStatus2(String movie) {
		try {
			stt.executeUpdate(
					"UPDATE `movies` SET `status` = 'available' WHERE `movies`.`ID` = '" + movid(movie) + "' ");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	void deleteMovie(String movie) {
		try {
			stt.executeUpdate("DELETE FROM `movies` WHERE `movies`.`ID` = '" + movid(movie) + "' ");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void addPayment(String id, String payment, String cash3) {
		try {
			stt.executeUpdate("INSERT INTO payment " + "VALUES ('" + id + "', '" + payment + "', '" + cash3 + "')");
			// System.out.print(id);
			// System.out.print(payment);
			// System.out.print(cash3);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void addResv(String usid, String movid) {
		String confirm;
		confirm = "no";
		try {

			stt.executeUpdate("INSERT INTO res " + "VALUES (NULL, '" + usid + "', '" + movid + "', '" + confirm + "')");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	boolean movieStatus(String movie) {
		try {
			res = stt.executeQuery("SELECT status FROM movies WHERE `movies`.`ID` = '" + movid(movie) + "' ");
			if (res.next()) {
				String status = res.getString("status");
				if (status.equals("available")) {
					return true;
				} else
					return false;
			} else
				return false;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	boolean resStatus(String reserv) {
		try {
			res = stt.executeQuery("SELECT confirm FROM res WHERE `ID` = '" + reserv + "' ");
			if (res.next()) {
				String status = res.getString("confirm");

				if (status.equals("NO") || status.equals("no")) {
					return true;
				} else
					return false;
			} else
				return false;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	int makeReservation(String user, String movie) {
		int ab = 0;
		try {
			if (movieStatus(movie)) {
				String usid, movid;
				usid = Integer.toString(usid(user));
				movid = Integer.toString(movid(movie));

				addResv(usid, movid);

				changeStatus(movie);
				ab = 1;

			} else {
				ab = 0;
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

		return ab;
	}

	int moviePrice(String movid) {
		int aa = 0;
		try {
			res = stt.executeQuery("SELECT price FROM movies WHERE `movies`.`ID` = '" + movid + "' ");
			while (res.next()) {
				int usida = res.getInt(1);
				aa = usida;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return aa;

	}

	int makeConfirmReservation(String cash, String id, String payment) {
		int ab = 0;
		try {
			if (resStatus(id)) {
				int cash2;
				cash2 = moviePrice(cash);
				String cash3;
				cash3 = Integer.toString(cash2);

				stt.executeUpdate("UPDATE `res` SET `confirm` = 'yes' WHERE `res`.`ID` = '" + id + "' ");
				// System.out.print(cash3);
				addPayment(id, payment, cash3);
				ab = 1;

			} else {
				ab = 0;
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

		return ab;
	}

}
