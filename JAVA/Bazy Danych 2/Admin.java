package Paczka;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class Admin extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	boolean logon = false;

	String url = "jdbc:mysql://localhost:3306";
	String dbuser = "root";
	String dbpassword = "";
	String auser = null;
	private JLabel ls;
	private JLabel ttl;
	private JLabel dir;
	private JTextField title;

	private JTextField director;
	private JButton filter;
	private JButton back2;

	private JButton res;
	private JButton stat;
	private JButton change;
	private DBCon con;
	private ResultSet result;

	private JLabel ttl2;
	private JTextField title2;
	private JButton add;
	private JLabel dir2;
	private JTextField dirr2;
	private JLabel date;
	private JTextField datee2;
	private JLabel genre;
	private JTextField genre2;
	private JLabel type;
	private JTextField type2;
	private JLabel price;
	private JTextField price2;
	private JTextField price3;

	Login windowe;
	GUI window;
	static Admin frame;

	public void setWindowse(Login asd, GUI cos) {
		windowe = asd;
		window = cos;

	}

	ArrayList<Reservation> resv;
	ArrayList<Movie> movies;
	DefaultTableModel model = new DefaultTableModel();
	private static Object[][] resvTable;
	private static Object[][] moviesTable;
	public static int countmovies = 0;
	String[] columns = { "Title", "Director", "Date", "Genre", "Type", "Status" };

	private JTable table;

	private String[] cond = new String[5];

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new Admin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @throws SQLException
	 */
	public Admin() throws SQLException {
		super("Logged as Admin");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING).addGap(0, 774, Short.MAX_VALUE));
		gl_contentPane.setVerticalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING).addGap(0, 551, Short.MAX_VALUE));
		contentPane.setLayout(gl_contentPane);

		movies = new ArrayList<Movie>();
		resv = new ArrayList<Reservation>();
		con = new DBCon(url, dbuser, dbpassword);

		ls = new JLabel(" ");
		ls.setFont(new Font("Tahoma", Font.BOLD, 14));
		ls.setLocation(5, 5);
		ls.setSize(160, 25);
		getContentPane().add(ls);

		ttl = new JLabel("Title:");
		ttl.setFont(new Font("Tahoma", Font.BOLD, 14));
		ttl.setLocation(5, 35);
		ttl.setSize(80, 25);
		getContentPane().add(ttl);
		ttl.setVisible(true);

		dir = new JLabel("Director:");
		dir.setFont(new Font("Tahoma", Font.BOLD, 14));
		dir.setLocation(130, 35);
		dir.setSize(80, 25);
		getContentPane().add(dir);
		dir.setVisible(true);

		title = new JTextField();
		title.setSize(120, 25);
		title.setLocation(5, 65);
		getContentPane().add(title);
		title.setVisible(true);

		director = new JTextField();
		director.setSize(120, 25);
		director.setLocation(130, 65);
		getContentPane().add(director);
		director.setVisible(true);

		result = con.getMovies();
		while (result.next()) {
			movies.add(new Movie(result.getString("title"), result.getString("director"), result.getInt("date"),
					result.getString("type"), result.getString("genre"), result.getString("status")));
		}
		moviesTable = new String[movies.size()][6];
		model.setDataVector(moviesTable, columns);
		countmovies = movies.size();
		for (int i = 0; i < movies.size(); i++) {
			moviesTable[i][0] = movies.get(i).title;
			moviesTable[i][1] = movies.get(i).director;
			moviesTable[i][2] = Integer.toString(movies.get(i).date);
			moviesTable[i][3] = movies.get(i).genre;
			moviesTable[i][4] = movies.get(i).type;
			moviesTable[i][5] = movies.get(i).status;
		}

		table = new JTable(moviesTable, columns);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setLocation(5, 95);
		scrollPane.setSize(575, 200);
		getContentPane().add(scrollPane);
		table.setFillsViewportHeight(true);

		filter = new JButton("Filter");
		filter.setFont(new Font("Tahoma", Font.BOLD, 14));
		filter.setSize(80, 25);
		filter.setLocation(425, 65);
		getContentPane().add(filter);
		filter.setVisible(true);

		back2 = new JButton("Logout");
		back2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				dispose();
				if (windowe != null) {
					windowe.setVisible(true);
					windowe.setWindows(window);

				}
			}
		});
		back2.setFont(new Font("Tahoma", Font.BOLD, 14));
		back2.setSize(150, 25);
		back2.setLocation(625, 35);
		getContentPane().add(back2);
		back2.setVisible(true);

		add = new JButton("Add Movie");
		add.setFont(new Font("Tahoma", Font.BOLD, 14));
		add.setSize(123, 25);
		add.setLocation(5, 425);
		getContentPane().add(add);
		add.setVisible(true);

		res = new JButton("Delete Movie");
		res.setFont(new Font("Tahoma", Font.BOLD, 14));
		res.setSize(180, 25);
		res.setLocation(5, 295);

		FilterHandler filterHandler = new FilterHandler();
		filter.addActionListener(filterHandler);
		filter.setVisible(true);

		ResHandler resHandler = new ResHandler();
		res.addActionListener(resHandler);
		getContentPane().add(res);
		res.setVisible(true);

		ResHandler2 resHandler2 = new ResHandler2();
		add.addActionListener(resHandler2);
		getContentPane().add(add);
		add.setVisible(true);

		ttl2 = new JLabel("Title:");
		ttl2.setFont(new Font("Tahoma", Font.BOLD, 14));
		ttl2.setLocation(5, 355);
		ttl2.setSize(80, 25);
		getContentPane().add(ttl2);
		ttl2.setVisible(true);

		title2 = new JTextField();
		title2.setSize(120, 25);
		title2.setLocation(5, 385);
		getContentPane().add(title2);
		title2.setVisible(true);

		dir2 = new JLabel("Director:");
		dir2.setFont(new Font("Tahoma", Font.BOLD, 14));
		dir2.setLocation(130, 355);
		dir2.setSize(80, 25);
		getContentPane().add(dir2);
		dir2.setVisible(true);

		dirr2 = new JTextField();
		dirr2.setSize(120, 25);
		dirr2.setLocation(130, 385);
		getContentPane().add(dirr2);
		dirr2.setVisible(true);

		date = new JLabel("Date:");
		date.setFont(new Font("Tahoma", Font.BOLD, 14));
		date.setLocation(255, 355);
		date.setSize(80, 25);
		getContentPane().add(date);
		date.setVisible(true);

		datee2 = new JTextField();
		datee2.setSize(120, 25);
		datee2.setLocation(255, 385);
		getContentPane().add(datee2);
		datee2.setVisible(true);

		genre = new JLabel("Genre:");
		genre.setFont(new Font("Tahoma", Font.BOLD, 14));
		genre.setLocation(380, 355);
		genre.setSize(80, 25);
		getContentPane().add(genre);
		genre.setVisible(true);

		genre2 = new JTextField();
		genre2.setSize(120, 25);
		genre2.setLocation(380, 385);
		getContentPane().add(genre2);
		genre2.setVisible(true);

		type = new JLabel("Type:");
		type.setFont(new Font("Tahoma", Font.BOLD, 14));
		type.setLocation(505, 355);
		type.setSize(80, 25);
		getContentPane().add(type);
		type.setVisible(true);

		type2 = new JTextField();
		type2.setSize(120, 25);
		type2.setLocation(505, 385);
		getContentPane().add(type2);
		type2.setVisible(true);

		price = new JLabel("Price:");
		price.setFont(new Font("Tahoma", Font.BOLD, 14));
		price.setLocation(630, 355);
		price.setSize(80, 25);
		getContentPane().add(price);
		price.setVisible(true);

		price2 = new JTextField();
		price2.setSize(120, 25);
		price2.setLocation(630, 385);
		getContentPane().add(price2);
		price2.setVisible(true);

		change = new JButton("Change status");
		change.setFont(new Font("Tahoma", Font.BOLD, 14));
		change.setSize(180, 25);
		change.setLocation(190, 295);

		ResHandler3 resHandler3 = new ResHandler3();
		change.addActionListener(resHandler3);
		getContentPane().add(change);
		change.setVisible(true);

		stat = new JButton("Statistics: ");
		stat.setFont(new Font("Tahoma", Font.BOLD, 14));
		stat.setSize(123, 25);
		stat.setLocation(5, 500);
		getContentPane().add(stat);

		ResHandler4 resHandler4 = new ResHandler4();
		stat.addActionListener(resHandler4);
		getContentPane().add(stat);
		stat.setVisible(true);
		
		price3 = new JTextField();
		price3.setSize(120, 25);
		price3.setLocation(130, 500);
		getContentPane().add(price3);
		price3.setVisible(true);
		

	}

	private class FilterHandler implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			table.setModel(model);
			if (title.getText().trim().isEmpty())
				cond[0] = null;
			else {
				cond[0] = title.getText();
				result = con.searchMoviesTitle(cond[0]);
			}
			if (director.getText().trim().isEmpty())
				cond[1] = null;
			else {
				cond[1] = director.getText();
				result = con.searchMoviesDirector(cond[1]);
			}
			if (title.getText().trim().isEmpty() & director.getText().trim().isEmpty()) {
				result = con.getMovies();
			}
			movies = new ArrayList<Movie>();
			try {
				while (result.next()) {
					movies.add(new Movie(result.getString("title"), result.getString("director"), result.getInt("date"),
							result.getString("type"), result.getString("genre"), result.getString("status")));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			for (int i = 0; i < movies.size(); i++) {

				table.setValueAt(movies.get(i).title, i, 0);
				table.setValueAt(movies.get(i).director, i, 1);
				table.setValueAt(Integer.toString(movies.get(i).date), i, 2);
				table.setValueAt(movies.get(i).genre, i, 3);
				table.setValueAt(movies.get(i).type, i, 4);
				table.setValueAt(movies.get(i).status, i, 5);

			}

		}
	}

	private class ResHandler implements ActionListener {
		public void actionPerformed(ActionEvent event) {

			if (movies.size() > 0) {
				String movie = (String) table.getValueAt(table.getSelectedRow(), 0);
				con.deleteMovie(movie);
			} else {
				JOptionPane.showMessageDialog(null, "Movie Base is Empty!!");
			}

		}
	}

	private class ResHandler2 implements ActionListener {
		public void actionPerformed(ActionEvent event) {

			String title, director, date, genre, type, price;
			title = title2.getText();
			director = dirr2.getText();
			date = datee2.getText();
			genre = genre2.getText();
			type = type2.getText();
			price = price2.getText();

			con.addMovie(title, director, date, genre, type, price);
		}
	}

	private class ResHandler3 implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (movies.size() > 0) {

				String movie = (String) table.getValueAt(table.getSelectedRow(), 0);
				String status = (String) table.getValueAt(table.getSelectedRow(), 5);
				String a = "available";
				if (status.equals(a)) {
					con.changeStatus(movie);
				} else {
					con.changeStatus2(movie);

				}
			} else {
				JOptionPane.showMessageDialog(null, "Movie Base is Empty!!");
			}

		}
	}

	private class ResHandler4 implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			result = con.getReservation();
			int price;
			price=0;
			try {
				while (result.next()) {
					resv.add(new Reservation(result.getString("ID"), result.getString("user_ID"),
							result.getString("movie_ID"), result.getString("confirm")));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			resvTable = new String[resv.size()][3];
			countmovies = resv.size();
			String a = "yes";
			for (int i = 0; i < resv.size(); i++) {
				if (resv.get(i).Confirm.equals(a)) {
					resvTable[i][1] = resv.get(i).Movie_ID;
					price=price+con.moviePrice(resv.get(i).Movie_ID);
				}

			}
			price3.setText(Integer.toString(price));
		}
	}
}
