#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <vector>

using namespace std;

class Komiwojazer
{
private:
	int** droga;
	int** a;
	int** b;
	int** tabuList;
	int dlugoscTabu;
	static int najkrotsza_sciezka;
	static int* najlepsza_droga;
	void permutacje(int*, int);
	int ** feromon;
	int ** czas;


public:
	double q;
	int mrowki;
	int ilosc_miast;									// ilosc miast
	int getIloscMiast();								// funkcja zwracajaca ilosc miast
	int getDroga(int, int);								// funkcja zwracająca drogę
	void setIloscMiast(int);							// funkcja ustawiająca ilosc miast
	void setDroga(int, int, int);						// funkcja ustawiająca drogę
	static void setNajkrotszaSciezka(int);				// funkcja  ustawiająca najkrótszą ścieżkę
	static int getNajkrotszaSciezka();					// funkcja zwracająca najkrótszą ścieżkę

	Komiwojazer();										// konstruktor klasy
	~Komiwojazer();										// destruktor klasy

	void wyswietl();									// funkcja wyświetlająca macierz
	void wygeneruj(int);								// funkcja generująca (parametr wejściowy ilośc miast)
	void wczytywanie(string);							// funkcja wczytująca (zadany parametr nazwa pliku bez rozszerzenia .txt)

	//Projekt SDiZO:
	void przegladZupelny();								// funkcja z algorytmem "Przegląd zupelny"


	//Projekt 1:
	void algorytmProgramowanieDynamiczne();				// Funkcja z algorytmem Programowania Dynamicznego
	void sciezka(int, int);								// Funkcja zwracająca najkrótszą ścieżkę
	int TSP(int, int);									// Funkcja obliczająca najkrótszą ścieżkę

	//Projekt 2:
	void algorytmTabuSearch(int);						// Funkcja z algorytmem Tabu Search
	int ccopy(int * dest, int * src, int leng);			// Funkcja kopiująca tablice
	int koszt(int *tab);								// Funkcja obliczająca koszt
	int *getBestNeighbour(int*tab2);					// Funkcja która zwraca listę z najlepszym sąsiedztwem
	int *swapaa(int city1, int city2, int *tab);		// Funkcja zamieniająca sąsiednie elementy tablicy 

	//Projekt 3:
	void algorytmGenetyczny(int, int);					// Przyjmuje jako argumenty populację oraz liczbę iteracji

	int* generujSciezke();								// Zwraca wygenerowaną ścieżkę
	int obliczDopasowanie(int*);						// Oblicza długość ścieżki określonego osobnika

	int* mutacjaInwersyjna(int*);						// Wykonuje na tablicy mutację inwersyjną na losowym przedziale
	int* mutacjaZamienna(int*);							// Wykonuje na tablicy mutację zamienną dwóch losowych genów
	
	int* krzyzowaniePMX(int*, int*);					// Wykonuje krzyżowanie dwóch osobników typu Partially Matched Crossover (Goldberg, Lingle - 1985)
	int* krzyzowanieOX(int*, int*);						// Wykonuje krzyżowanie dwóch osobników typu Order Crossover (Davis - 1985)

	//Projekt 4:
	void algorytmMrówkowy(int);							// Funkcja z algorytmem Mrówkowym
	int* trasa(int*);									// Funkcja wyznaczająca trase mrówki
	void aktualizacjaFeromonu(int*);					// Funkcja aktualizująca feromon na trasie
};
