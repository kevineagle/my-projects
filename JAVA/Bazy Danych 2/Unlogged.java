package Paczka;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.ResultSet;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.GroupLayout.Alignment;

public class Unlogged extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	boolean logon = false;

	String url = "jdbc:mysql://localhost:3306";
	String dbuser = "root";
	String dbpassword = "";
	String auser = null;

	private JLabel ttl;
	private JLabel dir;

	private JTextField title;
	private JTextField director;

	private JButton filter;
	private JButton back;
	private JButton add;
	private JButton remove;
	private DBCon con;
	private ResultSet result;

	GUI window;

	public void setWindows(GUI cos) {
		window = cos;

	}

	ArrayList<Movie> movies;
	DefaultTableModel model = new DefaultTableModel();
	private static Object[][] moviesTable;
	public static int countmovies;
	String[] columns = { "Title", "Director", "Date", "Genre", "Type", "Status" };

	private JTable table;

	private String[] cond = new String[5];
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Unlogged frame = new Unlogged();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @throws SQLException
	 */
	public Unlogged() throws SQLException {
		super("Przegladanie jako niezalogowany uzytkownik");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING).addGap(0, 774, Short.MAX_VALUE));
		gl_contentPane.setVerticalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING).addGap(0, 552, Short.MAX_VALUE));
		contentPane.setLayout(gl_contentPane);

		movies = new ArrayList<Movie>();
		con = new DBCon(url, dbuser, dbpassword);

		ttl = new JLabel("Title:");
		ttl.setFont(new Font("Tahoma", Font.BOLD, 14));
		ttl.setLocation(5, 5);
		ttl.setSize(80, 25);
		getContentPane().add(ttl);

		dir = new JLabel("Director:");
		dir.setFont(new Font("Tahoma", Font.BOLD, 14));
		dir.setLocation(130, 5);
		dir.setSize(80, 25);
		getContentPane().add(dir);

		title = new JTextField();
		title.setSize(120, 25);
		title.setLocation(5, 35);
		getContentPane().add(title);

		director = new JTextField();
		director.setSize(120, 25);
		director.setLocation(130, 35);
		getContentPane().add(director);

		result = con.getMovies();
		while (result.next()) {
			movies.add(new Movie(result.getString("title"), result.getString("director"), result.getInt("date"),
					result.getString("type"), result.getString("genre"), result.getString("status")));
		}
		moviesTable = new String[movies.size()][6];
		model.setDataVector(moviesTable, columns);
		countmovies=movies.size();
		for (int i = 0; i < movies.size(); i++) {
			moviesTable[i][0] = movies.get(i).title;
			moviesTable[i][1] = movies.get(i).director;
			moviesTable[i][2] = Integer.toString(movies.get(i).date);
			moviesTable[i][3] = movies.get(i).genre;
			moviesTable[i][4] = movies.get(i).type;
			moviesTable[i][5] = movies.get(i).status;
		}

		table = new JTable(moviesTable, columns);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setLocation(5, 65);
		scrollPane.setSize(750, 450);
		getContentPane().add(scrollPane);
		table.setFillsViewportHeight(true);

		filter = new JButton("Filter");
		filter.setFont(new Font("Tahoma", Font.BOLD, 14));
		filter.setSize(80, 25);
		filter.setLocation(425, 35);
		getContentPane().add(filter);

		back = new JButton("Back");
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				window.setVisible(true);
			}
		});
		back.setFont(new Font("Tahoma", Font.BOLD, 14));
		back.setSize(80, 25);
		back.setLocation(675, 35);
		getContentPane().add(back);

		add = new JButton("Add Movie");
		add.setFont(new Font("Tahoma", Font.BOLD, 14));
		add.setSize(123, 25);
		add.setLocation(455, 495);

		remove = new JButton("Remove Movie");
		remove.setFont(new Font("Tahoma", Font.BOLD, 14));
		remove.setSize(123, 25);
		remove.setLocation(455, 525);

		FilterHandler filterHandler = new FilterHandler();
		filter.addActionListener(filterHandler);

	}

	private class FilterHandler implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			table.setModel(new DefaultTableModel(null, columns));
			moviesTable = new String[countmovies][6];
			model.setDataVector(moviesTable, columns);
			table.setModel(model);
			if (title.getText().trim().isEmpty())
				cond[0] = null;
			else {
				cond[0] = title.getText();
				result = con.searchMoviesTitle(cond[0]);
			}
			if (director.getText().trim().isEmpty())
				cond[1] = null;
			else {
				cond[1] = director.getText();
				result = con.searchMoviesDirector(cond[1]);
			}
			if (title.getText().trim().isEmpty() & director.getText().trim().isEmpty()) {
				result = con.getMovies();
			}
			movies = new ArrayList<Movie>();
			try {
				while (result.next()) {
					movies.add(new Movie(result.getString("title"), result.getString("director"), result.getInt("date"),
							result.getString("type"), result.getString("genre"), result.getString("status")));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			for (int i = 0; i < movies.size(); i++) {

				table.setValueAt(movies.get(i).title, i, 0);
				table.setValueAt(movies.get(i).director, i, 1);
				table.setValueAt(Integer.toString(movies.get(i).date), i, 2);
				table.setValueAt(movies.get(i).genre, i, 3);
				table.setValueAt(movies.get(i).type, i, 4);
				table.setValueAt(movies.get(i).status, i, 5);

			}
		}
	}

}
