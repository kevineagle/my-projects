
package lab06_pop;

import java.net.*;
import java.io.*;
import javax.swing.*;
import java.awt.event.*;

class Monitor extends JFrame implements ActionListener, Runnable {

	private static final long serialVersionUID = 1L;

	private JLabel e_text = new JLabel("Stan:");
	private JComboBox<Object> menuKlient = new JComboBox<Object>();
	private JTextField message = new JTextField(20);
	private JTextArea textArea = new JTextArea(15, 18);
	private JScrollPane scroll = new JScrollPane(textArea, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

	static final int SERVER_PORT = 15000;
	private String host;
	private ServerSocket serwer;

	Monitor() {
		super("Monitor");
		setSize(300, 340);
		this.setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JPanel panel = new JPanel();

		message.addActionListener(this);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		panel.add(e_text);
		textArea.setEditable(false);
		panel.add(scroll);
		setContentPane(panel);
		Thread t = new Thread(this); // Uruchomienie dodatkowego watka
		t.start();
		setVisible(true);
	}

	synchronized public void wypiszOdebrane(Watek watek1, String s) {
		String pom = textArea.getText();
		textArea.setText(watek1.getNazwa() + " > " + s + "\n" + pom);
	}

	public void actionPerformed(ActionEvent evt) {
		String m;
		Object src = evt.getSource();
		if (src == message) {
			Watek k = (Watek) menuKlient.getSelectedItem();
			if (k != null) {
				try {
					m = message.getText();
					k.getOutput().writeObject(m);

					if (m.equals("exit")) {

					}
				} catch (IOException e) {
				}
			}
		}
		repaint();
	}

	public void run() {
		Socket s;
		Watek k;

		try {
			host = InetAddress.getLocalHost().getHostName();
			serwer = new ServerSocket(SERVER_PORT);
		} catch (IOException e) {

		}

		while (true) {
			try {
				s = serwer.accept();
				if (s != null) {
					k = new Watek(this, s);
				}
			} catch (IOException e) {
			}
		}
	}

	public static void main(String[] args) {
		new Monitor();
	}

}

class Watek implements Runnable {
	private Socket socket;
	private ObjectOutputStream output;
	private ObjectInputStream input;

	private String nazwa;
	private Monitor okno;

	Watek(Monitor mon, Socket s) throws IOException {
		okno = mon;
		socket = s;
		Thread t = new Thread(this); // Utworzenie dodatkowego watka
		t.start(); // do obslugi komunikacji sieciowej
	}

	public String getNazwa() {
		return nazwa;
	}

	public ObjectOutputStream getOutput() {
		return output;
	}

	public String toString() {
		return nazwa;
	}

	public void run() {
		String m, pom;
		try {
			output = new ObjectOutputStream(socket.getOutputStream());
			input = new ObjectInputStream(socket.getInputStream());
			nazwa = (String) input.readObject();

			while (true) {
				m = (String) input.readObject();
				okno.wypiszOdebrane(this, m);
				if (m.equals("exit")) {
					break;
				}
			}
			input.close();
			output.close();
			socket.close();
			socket = null;
		} catch (Exception e) {
		}
	}
}
