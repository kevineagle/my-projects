package Paczka;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.EmptyBorder;

public class Login extends JFrame {
	// static Loginin frame;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	boolean logon = false;

	String url = "jdbc:mysql://localhost:3306";
	String dbuser = "root";
	String dbpassword = "";
	String auser = null;

	private JLabel l;
	private JLabel p;
	private JLabel ls;

	private JTextField un;
	private JTextField pw;
	private JButton back;
	private JButton login;
	private DBCon con;
	GUI window;
	static Login framese;

	public void setWindows(GUI cos) {
		window = cos;

	}

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public Login() {
		super("Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING).addGap(0, 774, Short.MAX_VALUE));
		gl_contentPane.setVerticalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING).addGap(0, 551, Short.MAX_VALUE));
		contentPane.setLayout(gl_contentPane);

		new DBCon(url, dbuser, dbpassword);
		con = new DBCon(url, dbuser, dbpassword);
		l = new JLabel("Username: ");
		l.setFont(new Font("Tahoma", Font.BOLD, 14));
		l.setLocation(215, 175);
		l.setSize(80, 25);
		getContentPane().add(l);

		p = new JLabel("Password: ");
		p.setFont(new Font("Tahoma", Font.BOLD, 14));
		p.setLocation(215, 215);
		p.setSize(80, 25);
		getContentPane().add(p);

		ls = new JLabel(" ");
		ls.setFont(new Font("Tahoma", Font.BOLD, 14));
		ls.setLocation(5, 5);
		ls.setSize(160, 25);
		getContentPane().add(ls);

		un = new JTextField();
		un.setSize(120, 25);
		un.setLocation(305, 175);
		getContentPane().add(un);

		pw = new JTextField();
		pw.setSize(120, 25);
		pw.setLocation(305, 215);
		getContentPane().add(pw);

		
		
		
		back = new JButton("Back");
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				if (window != null)
					window.setVisible(true);
			}
		});
		back.setFont(new Font("Tahoma", Font.BOLD, 14));
		back.setSize(120, 25);
		back.setLocation(305, 295);
		getContentPane().add(back);

		////
		login = new JButton("Login");
		login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String user = un.getText();
				String pass = pw.getText();
				if (con.userType(user)) {
					Admin logg = null;
					try {

						framese = new Login();
						logg = new Admin();
						logg.setWindowse(framese, window);

					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					logon = con.authenticateUser(user, pass);
					if (logon) {

						logg.setVisible(true);
						dispose();

					} else {
						JOptionPane.showMessageDialog(null, "Wrong username or password");
					}
				} else {
					User logg = null;
					try {

						framese = new Login();
						logg = new User();
						logg.setWindowse(framese, window, user);

					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					logon = con.authenticateUser(user, pass);
					if (logon) {

						logg.setVisible(true);
						dispose();

					} else {
						JOptionPane.showMessageDialog(null, "Wrong username or password");
					}
				}

			}
		});
		login.setFont(new Font("Tahoma", Font.BOLD, 14));
		login.setSize(120, 25);
		login.setLocation(305, 255);
		getContentPane().add(login);

	}

}
