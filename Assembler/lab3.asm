.text
.global symbol_newtona
.type symbol_newtona, @function


# Deklaracja w C: symbol_newtona(n, k);
#
symbol_newtona:
poczatek:
push %rbp
mov %rsp, %rbp

mov %rsi, %rax  #Pierwszy argument n
push %rax

call silnia
mov %rax, %r8

mov %rdi, %rax #Drugi argument k
mov %rdi, %r10
push %rax
info:
call silnia
psilnia:
mov %rax, %r9

sub %rsi, %r10
odejmowanie:
mov %r10, %rax
push %rax

call silnia
nminus:
imul %rax, %r8
mnozenie:
mov %r9, %rax
prz:
cdq
div %r8

mov %rbp, %rsp
pop %rbp
wynik:

ret # powrot do miejsca wywolania funkcji


silnia:
push %rbp
mov %rsp, %rbp
mov 16(%rbp), %rbx
cmp $1, %rbx
je koniec_rekurencji
dec %rbx
push %rbx
call silnia
mov 16(%rbp), %rbx
imul %rbx, %rax
jmp koniec
koniec_rekurencji:
mov $1, %rax
koniec:
mov %rbp, %rsp
pop %rbp
ret
