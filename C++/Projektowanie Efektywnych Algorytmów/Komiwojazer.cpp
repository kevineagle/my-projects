#include "stdafx.h"
#include "Komiwojazer.h"
#include <algorithm>
#include <iostream>
Komiwojazer::Komiwojazer()
{
	ilosc_miast = 0;
	q = 0.5000;
	// Konstruktor klasy komiwojazer
}
Komiwojazer::~Komiwojazer()
{
	for (int i = 0; i < ilosc_miast; i++)
	{
		delete[] droga[i];																	// Destuktor klasy komiwojazer
	}
	delete[] droga;
}
int Komiwojazer::najkrotsza_sciezka = 0;
int* Komiwojazer::najlepsza_droga = NULL;
void Komiwojazer::setIloscMiast(int ilosc_miast)
{
	this->ilosc_miast = ilosc_miast;															// Funkcja ustawiająca ilośc miast
}
void Komiwojazer::setDroga(int i, int j, int odleglosc)
{
	this->droga[i][j] = odleglosc;															// Funkcja ustawiająca odległośc
}
void Komiwojazer::setNajkrotszaSciezka(int najkrotsza_sciezka)
{
	Komiwojazer::najkrotsza_sciezka = najkrotsza_sciezka;										// Funkcja ustawiająca najkrótszą ściężkę
}
int Komiwojazer::getIloscMiast()
{
	return ilosc_miast;																			// Funkcja zwracajaca ilośc miast
}
int Komiwojazer::getDroga(int i, int j)
{
	return droga[i][j];																		// Funkcja zwracająca odległość
}
int Komiwojazer::getNajkrotszaSciezka()
{
	return Komiwojazer::najkrotsza_sciezka;														// Funkcja zwracająca najkrótszą ścieżkę
	return 0;
}
void Komiwojazer::wyswietl()
{
	// Funkcja wyswietlajaca aktualne dane
	cout << endl << "Ilosc miast: " << getIloscMiast() << endl << endl;
	for (int i = 0; i < getIloscMiast(); i++)
	{
		for (int j = 0; j < getIloscMiast(); j++)
		{
			cout << setw(4) << droga[i][j] << " ";
		}
		cout << endl << endl;
	}
}
void Komiwojazer::wygeneruj(int ilosc)
{
	if (getIloscMiast() != 0)																		// Jeżeli ilosc miast różna od zera (trzeba je wykasować)
	{
		for (int i = 0; i < ilosc_miast; i++)
		{
			delete[] droga[i];
		}
		delete[] droga;
	}

	if (ilosc <= 0)																				// Funkcja generująca miasta przy zadanej ich ilosc (przez uzytkownika)
	{
		cout << "Nieprawidlowa ilosc miast!" << endl;
		return;
	}

	setIloscMiast(ilosc);																		// Ustawienie ilości miast
	droga = new int*[ilosc];																// Stworzenie tablicy wskaźników
	for (int i = 0; i < ilosc; i++)
	{
		droga[i] = new int[ilosc];
	}
	for (int i = 0; i < ilosc; i++)
	{
		for (int j = 0; j < ilosc; j++)
		{
			droga[i][j] = 0;																// Wypełnienie całej tablicy zerami
		}
	}

	for (int i = 0; i < ilosc; i++)
	{
		for (int j = 0; j < ilosc; j++)
		{
			if (i != j)
			{
				droga[i][j] = (rand() % 99) + 1;
			}									// Wylosowanie odległości w zakresie od 1 do 99 (za wyjatkiem przekatnych ponieważ odległość z miasta a do a jest rónwa 0
		}
	}
}
void Komiwojazer::wczytywanie(string nazwa)
{
	if (getIloscMiast() != 0)
	{
		for (int i = 0; i < getIloscMiast(); i++)
		{
			delete[] droga[i];
		}
		delete[] droga;
	}
	ifstream file;
	nazwa += ".txt";
	file.open(nazwa);

	if (!file.good())
	{
		cout << "Nie ma takiego pliku." << endl;
		return;
	}

	int pomoc = 0;
	file >> pomoc;
	setIloscMiast(pomoc);

	droga = new int*[getIloscMiast()];

	for (int i = 0; i < getIloscMiast(); i++)
	{
		droga[i] = new int[getIloscMiast()];
	}


	for (int i = 0; i < getIloscMiast(); i++)
	{
		for (int j = 0; j < getIloscMiast(); j++)
		{
			file >> droga[i][j];

		}
	}

	file.close();
}

//Algorytm Przegląd Zupełny:
void Komiwojazer::permutacje(int* tablica, int indeks)
{
	// Funkcja generujaca permutacje dla przegladu zupelnego
	if (indeks < (ilosc_miast - 1))
	{
		for (int i = indeks; i < ilosc_miast; i++)
		{
			swap(tablica[indeks], tablica[i]);
			permutacje(tablica, indeks + 1);
			swap(tablica[indeks], tablica[i]);
		}
	}
	else
	{
		int dlugosc_trasy = 0;
		for (int i = 0; i < (ilosc_miast - 1); i++)
		{
			dlugosc_trasy += droga[tablica[i]][tablica[i + 1]];
		}
		dlugosc_trasy += droga[tablica[ilosc_miast - 1]][tablica[0]];

		if (dlugosc_trasy < najkrotsza_sciezka)
		{
			najkrotsza_sciezka = dlugosc_trasy;
			for (int i = 0; i < ilosc_miast; i++)
			{
				najlepsza_droga[i] = tablica[i];
			}
		}


	}
}
void Komiwojazer::przegladZupelny()
{
	// ALGORYTM - Przeglad zupelny 

	cout << "                PRZEGLAD ZUPELNY:              " << endl;
	if (getIloscMiast() < 2) // === WARUNEK ROZPOCZĘCIA ALGORYTMU === //
	{
		cout << "Nie da sie uruchomic algorytmu dla instancji mniejszej od 2!" << endl;
		return;
	}
	int *tablica_miast = new int[getIloscMiast()];
	for (int i = 0; i < getIloscMiast(); i++)
	{
		tablica_miast[i] = i;
	}
	najkrotsza_sciezka = INT_MAX;
	najlepsza_droga = new int[getIloscMiast()];

	wyswietl();
	permutacje(tablica_miast, 1);
	cout << "ROZWIAZANIE" << endl;
	cout << "Calkowita odleglosc: " << najkrotsza_sciezka << endl;
	cout << "Sciezka: ";
	for (int i = 0; i < getIloscMiast(); i++)
	{
		cout << najlepsza_droga[i] << " --> ";
	}
	cout << najlepsza_droga[0] << endl << endl;

	delete[]tablica_miast;
}

//Algorytm Programowania Dynamicznego
void Komiwojazer::algorytmProgramowanieDynamiczne()
{
	cout << "                PRZEGLAD ZUPELNY:              " << endl;
	if (getIloscMiast() < 2) // === WARUNEK ROZPOCZĘCIA ALGORYTMU === //
	{
		cout << "Nie da sie uruchomic algorytmu dla instancji mniejszej od 2!" << endl;
		return;
	}
	a = new int*[getIloscMiast()];
	b = new int*[getIloscMiast()];

	for (int i = 0; i < getIloscMiast(); i++)
	{
		a[i] = new int[2 << getIloscMiast()];
		b[i] = new int[2 << getIloscMiast()];
	}
	for (int i = 0; i < getIloscMiast(); i++)
	{
		for (int j = 0; j < 1 << (getIloscMiast() + 1); j++)
		{
			a[i][j] = -1;
			b[i][j] = -1;
		}
	}
	for (int i = 0; i < getIloscMiast(); i++)
	{
		a[i][0] = droga[i][0];
	}
	najkrotsza_sciezka = TSP(0, (2 << getIloscMiast()) - 2);
	cout << "Koszt: " << najkrotsza_sciezka << endl;
	cout << "Sciezka: ";
	sciezka(0, (2 << getIloscMiast()) - 2);
	for (int i = 0; i < getIloscMiast(); i++)
	{
		delete[] a[i];
		delete[] b[i];
	}
	delete[] a;
	delete[] b;
	return;
}
void Komiwojazer::sciezka(int start, int set)
{
	if (b[start][set] == -1)
	{
		cout << set;
		return;
	}
	int x = b[start][set];
	int mask = (2 << getIloscMiast()) - 1 - (int)(2 << x);
	int masked = set & mask;
	cout << x << " -> ";
	if (masked != set)
	{

		sciezka(x, masked);
	}
}
int Komiwojazer::TSP(int start, int set)
{
	int masked, mask, result = -1, temp;
	if (a[start][set] != -1)
	{
		return a[start][set];
	}
	else
	{
		for (int x = 0; x < getIloscMiast(); x++)
		{
			mask = (2 << getIloscMiast()) - 1 - (int)(2 << x);
			masked = set & mask;
			if (masked != set)
			{
				temp = droga[start][x] + TSP(x, masked);
				if (result == -1 || result > temp)
				{
					result = temp;
					b[start][set] = x;
				}

			}
		}
		a[start][set] = result;
		return result;
	}
}

//Algorytm Tabu Search:
void Komiwojazer::algorytmTabuSearch(int tabu)
{
	cout << "                TABU SEARCH:              " << endl;
	if (getIloscMiast() < 2) // === WARUNEK ROZPOCZĘCIA ALGORYTMU === //
	{
		cout << "Nie da sie uruchomic algorytmu dla instancji mniejszej od 2!" << endl;
		return;
	}

	int *obecneRozwiazanie = new int[getIloscMiast() + 1]; // inicjalizowanie rozwiazania
	for (int i = 0; i < getIloscMiast(); i++)
	{
		obecneRozwiazanie[i] = i;						// inicjalizowanie rozwiazania od pierwszego miasta do ostatniego
	}
	obecneRozwiazanie[getIloscMiast()] = 0;				// inicjalizowania rozwiazania, powrót z ostatniego miasta do pierwszego
	int liczbaIteracji = 100;
	dlugoscTabu = tabu;
	if (dlugoscTabu < getIloscMiast())
	{
		dlugoscTabu = getIloscMiast() + 1;
	}
	tabuList = new int*[dlugoscTabu];					// inicjalizowanie listy tabu
	for (int i = 0; i < dlugoscTabu; i++)
	{
		tabuList[i] = new int[dlugoscTabu];

	}
	for (int i = 0; i < dlugoscTabu; i++)
	{
		for (int j = 0; j < dlugoscTabu; j++)
		{
			tabuList[i][j] = 0;							// Przypisanie zer
		}
	}
	int *najlepszeRozwiazanie = new int[getIloscMiast() + 1]; // najlepsze rozwiazanie
	*najlepszeRozwiazanie = ccopy(najlepszeRozwiazanie, obecneRozwiazanie, getIloscMiast() + 1); //przypisanie obecnego rozwiazania, najlepszemu rozwiazaniu

	int najlepszyKoszt = koszt(najlepszeRozwiazanie); //przypisanie najlepszego kosztu poprzez funkcje koszt od najlepszego rozwiazania

	for (int i = 0; i < liczbaIteracji; i++)
	{
		// iteracja

		obecneRozwiazanie = getBestNeighbour(obecneRozwiazanie);

		int obecnyKoszt = koszt(obecneRozwiazanie);

		if (obecnyKoszt < najlepszyKoszt)
		{
			//Jesli obecny koszt jest mniejszy od najlepszego dotychczas nastepuje przypisanie obecnego rozwiazania jako najlepszego
			*najlepszeRozwiazanie = ccopy(najlepszeRozwiazanie, obecneRozwiazanie, getIloscMiast() + 1);
			najlepszyKoszt = obecnyKoszt;
		}
	}

	//Wyswietlanie kosztu i rozwiazania
	cout << endl << endl;
	cout << "Koszt: " << najlepszyKoszt << endl;
	cout << "Sciezka: ";
	for (int i = 0; i < getIloscMiast() + 1; i++)
	{
		cout << najlepszeRozwiazanie[i];
		if (i < getIloscMiast())
		{
			cout << " -> ";
		}


	}
	cout << endl;


}
int Komiwojazer::ccopy(int * mPrzeznaczenia, int * zrodlo, int dlugosc)
{
	//Funkcja kopiujaca jedna tablice do drugiej
	for (int i = 0; i < dlugosc; i++)
	{
		mPrzeznaczenia[i] = zrodlo[i];

	}
	return *mPrzeznaczenia;
}
int Komiwojazer::koszt(int * tab)
{
	//Funkcja wyliczajaca koszt dla danego rozwiazania, parametrem jest tablica zawierajaca koszt pomiedzy danymi miastami
	int koszt = 0;

	for (int i = 0; i < getIloscMiast(); i++)
	{
		koszt += droga[tab[i]][tab[i + 1]]; //Dodanie kosztu miedzy miastem obecnym a nastepnym do sumy
	}

	return koszt; //funkcja zwraca koszt (liczba calkowita)
}
int * Komiwojazer::getBestNeighbour(int * tab2)
{
	int *najlepszeRozwiazanie = new int[getIloscMiast() + 1];
	*najlepszeRozwiazanie = ccopy(najlepszeRozwiazanie, tab2, getIloscMiast() + 1);

	int najlepszyKoszt = koszt(tab2);
	int miasto1 = 0;
	int miasto2 = 0;
	bool pierwszySasiad = true;

	for (int i = 1; i < getIloscMiast(); i++)
	{
		for (int j = 1; j < getIloscMiast(); j++)
		{
			if (i == j)
			{
				continue;
			}
			int *noweRozwiazanie = new int[getIloscMiast() + 1];
			*noweRozwiazanie = ccopy(noweRozwiazanie, tab2, getIloscMiast() + 1);

			noweRozwiazanie = swapaa(i, j, tab2); // Generowanie nowego rozwiazania poprzez funkcje swapaa która zamienia


			int nowyKoszt = koszt(noweRozwiazanie);


			if ((nowyKoszt < najlepszyKoszt || pierwszySasiad) && tabuList[i][j] == 0)
			{   //Jesli nowy koszt jest mniejszy od najlepszego do tej pory kosztu i nie nie znajduje sie na liscie tabu lub pierwszy sasiad jest true i nie znajduje sie na liscie tabu
				//nastepuje przypisanie za najlepsze rozwiazanie i kosztu, tego nowego rozwiazania
				pierwszySasiad = false;
				miasto1 = i;
				miasto2 = j;
				*najlepszeRozwiazanie = ccopy(najlepszeRozwiazanie, noweRozwiazanie, getIloscMiast() + 1);
				najlepszyKoszt = nowyKoszt;
				tabuList[miasto1][miasto2] += 1;
			}


		}
	}


	for (int i = 0; i < dlugoscTabu; i++)
	{
		for (int j = 0; j < dlugoscTabu; j++)
		{
			tabuList[i][j] = 0;
		}
	}



	return najlepszeRozwiazanie;
}
int * Komiwojazer::swapaa(int city1, int city2, int * tab)
{
	int temp = tab[city1];
	tab[city1] = tab[city2];
	tab[city2] = temp;
	return tab;
}

//Algorytm Genetyczny:
int* Komiwojazer::generujSciezke()
{
	// Losowanie sciezki dla TSP poprzez przemieszanie
	int* sciezka_nowa = new int[getIloscMiast()];
	for (int i = 0; i < getIloscMiast(); i++)
	{
		sciezka_nowa[i] = i;
	}

	int gen_1 = 0, gen_2 = 0;
	for (int i = 0; i < getIloscMiast(); i++)
	{
		gen_1 = (rand() % getIloscMiast());
		gen_2 = (rand() % getIloscMiast());
		if (gen_1 != 0 && gen_2 != 0)
		{
			swap(sciezka_nowa[gen_1], sciezka_nowa[gen_2]);
		}
	}
	return sciezka_nowa;
}
int Komiwojazer::obliczDopasowanie(int* osobnik)
{
	// Obliczanie wartości dopasowania określonego osobnika	
	int dopasowanie = 0;
	for (int i = 0; i < getIloscMiast() - 1; i++)
	{
		dopasowanie += droga[osobnik[i]][osobnik[i + 1]];

	}
	dopasowanie += droga[osobnik[getIloscMiast() - 1]][osobnik[0]];
	return dopasowanie;
}
int* Komiwojazer::mutacjaInwersyjna(int* osobnik)
{
	// Mutacja poprzez inwersję losowego przedziału	
	int gen_1 = 0, gen_2 = 0;
	gen_1 = rand() % ilosc_miast;
	gen_2 = rand() % ilosc_miast;

	// Upewniamy się, że wybraliśmy dwie różne pozycje
	while (gen_1 == gen_2)
	{
		gen_2 = rand() % ilosc_miast;
	}

	if (gen_1 > gen_2)
	{
		swap(gen_1, gen_2);
	}

	// Zmienne pomocnicze do szybkiej inwersji
	int ilosc_zamian = 0;
	int gen_od_lewej = gen_1, gen_od_prawej = gen_2;
	if ((gen_2 - gen_1) % 2 == 1)
	{
		ilosc_zamian = (int)ceil((gen_2 - gen_1) / 2);  // ilosc zmiany = zaokraglenie górne((gen2-gen1)/2)
	}
	else
	{
		ilosc_zamian = (gen_2 - gen_1) / 2;
	}

	// Mutacja
	do
	{
		swap(osobnik[gen_od_lewej], osobnik[gen_od_prawej]);
		gen_od_lewej++;
		gen_od_prawej--;
		ilosc_zamian--;
	} while (ilosc_zamian >= 0);

	return osobnik;
}
int* Komiwojazer::mutacjaZamienna(int* osobnik)
{
	// Mutacja poprzez zamianę losowych genów		
	int gen_1 = 0, gen_2 = 0;
	gen_1 = rand() % ilosc_miast;
	gen_2 = rand() % ilosc_miast;

	// Upewniamy się, że wybraliśmy dwie różne pozycje
	while (gen_1 == gen_2)
	{
		gen_2 = rand() % ilosc_miast;
	}

	// Mutacja
	if (gen_1 != 0 && gen_2 != 0)
	{
		swap(osobnik[gen_1], osobnik[gen_2]);
		return osobnik;
	}

}
int* Komiwojazer::krzyzowaniePMX(int* matka, int* ojciec)
{
	int* syn = new int[ilosc_miast];
	for (int i = 0; i < ilosc_miast; i++) syn[i] = -1;

	// Wybieramy przedzial krzyzowania
	int gen_1 = 0, gen_2 = 0;
	gen_1 = rand() % ilosc_miast;
	gen_2 = rand() % ilosc_miast;

	// Upewniamy się, że wybraliśmy dwie różne pozycje
	while (gen_1 == gen_2)
	{
		gen_2 = rand() % ilosc_miast;
	}

	if (gen_1 > gen_2)
	{
		swap(gen_1, gen_2);
	}

	// Kopiujemy do syna wylosowany przedział genów matki
	copy(matka + gen_1, matka + gen_2 + 1, syn + gen_1);

	// Sprawdzamy czy nie przekopiowaliśmy już przypadkiem wszystkich genów matki
	if (gen_1 != 0 || gen_2 != ilosc_miast - 1)
	{
		// Wektor genów ojca z wylosowanego przedziału których nie posiada syn
		vector<int*> nieskopiowane;

		for (int i = gen_1; i <= gen_2; i++)
		{
			bool zawiera_gen = false;
			for (int j = gen_1; j <= gen_2; j++)
			{
				if (ojciec[i] == syn[j])
				{
					zawiera_gen = true;
					break;
				}
			}

			if (!zawiera_gen)
			{
				nieskopiowane.push_back(new int[2]{ ojciec[i], matka[i] });
			}
		}

		int pozycja_genu_matki;
		for (int i = 0; i < (signed int)nieskopiowane.size(); i++)
		{
			// Wyznaczanie pozycji do ktorej skopiujemy brakujace geny ojca
			for (int j = 0; j < ilosc_miast; j++)
			{
				if (nieskopiowane[i][1] == ojciec[j])
				{
					pozycja_genu_matki = j;
					break;
				}
			}

			// Mamy wolne miejsce
			if (pozycja_genu_matki < gen_1 || pozycja_genu_matki > gen_2)
			{
				syn[pozycja_genu_matki] = nieskopiowane[i][0];
			}
			else
			{
				// Musimy wlozyc brakujacy gen w inne wolne miejsce
				while (true)
				{
					for (int j = 0; j < ilosc_miast; j++)
					{
						if (ojciec[j] == matka[pozycja_genu_matki])
						{
							pozycja_genu_matki = j;
							break;
						}
					}

					if (pozycja_genu_matki < gen_1 || pozycja_genu_matki > gen_2)
					{
						break;
					}
				}

				syn[pozycja_genu_matki] = nieskopiowane[i][0];
			}
		}

		// Przekopiowanie reszty brakjących genów
		for (int i = 0; i < ilosc_miast; i++)
		{
			if (syn[i] == -1)
			{
				syn[i] = ojciec[i];
			}
		}
	}

	return syn;
}
int* Komiwojazer::krzyzowanieOX(int* matka, int* ojciec)
{
	int* syn = new int[ilosc_miast];
	for (int i = 0; i < ilosc_miast; i++) syn[i] = -1;

	// Wybieramy przedzial krzyzowania
	int gen_1 = 0, gen_2 = 0;
	gen_1 = rand() % ilosc_miast;
	gen_2 = rand() % ilosc_miast;

	// Upewniamy się, że wybraliśmy dwie różne pozycje
	while (gen_1 == gen_2) gen_2 = rand() % ilosc_miast;

	if (gen_1 > gen_2) swap(gen_1, gen_2);

	// Kopiujemy do syna wylosowany przedział genów matki
	copy(matka + gen_1, matka + gen_2 + 1, syn + gen_1);
	// Sprawdzamy czy nie przekopiowaliśmy już przypadkiem wszystkich genów matki
	if (gen_1 != 0 || gen_2 != ilosc_miast - 1) {
		// Geny ojca których nie posiada syn 
		vector<int> nieskopiowane;

		// Wyznaczenie pozycji do rozpoczęcia poszukiwań niewykorzystanych genów
		int poszukiwacz = gen_2 + 1;
		if (poszukiwacz == ilosc_miast) poszukiwacz = 0;

		// Wyznaczenie ilości genów które musimy odnaleźć
		int do_skopiowania = ilosc_miast - (gen_2 - gen_1 + 1);

		// Poszukiwanie zaginionych genów po skopiowanym segmencie
		do {
			bool kopiuj = true;
			if (poszukiwacz == ilosc_miast) poszukiwacz = 0;
			for (int i = gen_1; i <= gen_2; i++) {
				if (ojciec[poszukiwacz] == matka[i]) {
					kopiuj = false;
					break;
				}
			}

			if (kopiuj) {
				nieskopiowane.push_back(ojciec[poszukiwacz]);
				do_skopiowania--;
			}

			poszukiwacz++;
		} while (do_skopiowania > 0);
		int pozycja = 0;
		for (int i = gen_2 + 1; i < ilosc_miast; i++) syn[i] = nieskopiowane[pozycja++];
		for (int i = 0; i < gen_1; i++) syn[i] = nieskopiowane[pozycja++];
	}

	return syn;
}
void Komiwojazer::algorytmGenetyczny(int populacja, int iteracje)
{
	cout << "                ALGORYTM GENETYCZNY:              " << endl;
	if (getIloscMiast() < 2) // === WARUNEK ROZPOCZĘCIA ALGORYTMU === //
	{
		cout << "Nie da sie uruchomic algorytmu dla instancji mniejszej od 2!" << endl;
		return;
	}


	int** osobnik = new int*[populacja];							// Inicjalizacja osobników - każdy odzwierciedla pełną losowo wygenerowaną ścieżkę
	for (int i = 0; i < populacja; i++)
	{
		osobnik[i] = generujSciezke();
	}

	int* dopasowanie = new int[populacja];							// Tablica przechowujaca informacje o dopasowaniu osobnika w TSP (mniejsza wartosc - lepsze dopasowanie)
	int* najlepszy_osobnik = new int[getIloscMiast()];				// Tablica przechowujaca ścieżkę najlepszego dopasowania
	int najlepsza_sciezka = INT_MAX;								// Zmienna przechowująca wartość najlepszego dopasowania
	int dopasowanie_syna;

	// Obliczanie dopasowania osobników startowych
	for (int i = 0; i < populacja; i++)
	{
		dopasowanie[i] = obliczDopasowanie(osobnik[i]);

		// Mamy lepszego osobnika
		if (dopasowanie[i] < najlepsza_sciezka)
		{
			for (int j = 0; j < getIloscMiast(); j++)
			{
				najlepszy_osobnik[j] = osobnik[i][j];
			}

			najlepsza_sciezka = dopasowanie[i];
		}
	}

	int* nowy_osobnik;
	int osobnik_1 = 0, osobnik_2 = 0;

	for (int iteracja = 0; iteracja < iteracje; iteracja++)
	{
		// Wyznaczanie najlepszego osobnika z aktualnej populacji i zapisanie go do tablicy 
		// najlepszy_osobnik jeżeli okazał się on być najlepszym jak do tej pory
		for (int i = 0; i < populacja; i += 2)
		{
			// Wybieramy dwóch osobników do skrzyżowania
			osobnik_1 = rand() % populacja;
			osobnik_2 = rand() % populacja;
			while (osobnik_1 == osobnik_2)
			{
				osobnik_2 = rand() % populacja;
			}

			// Tworzymy nowego osobnika z ich genów
			nowy_osobnik = krzyzowanieOX(osobnik[osobnik_1], osobnik[osobnik_2]);					// Wybór rodzaju krzyżowania / mutacji

																									// Zamieniamy nowego osobnika ze słabszym z rodziców
			dopasowanie_syna = obliczDopasowanie(nowy_osobnik);
			if (dopasowanie[osobnik_1] < dopasowanie[osobnik_2])
			{																						// Pierwszy osobnik okazał się być słabszy
				osobnik[osobnik_1] = nowy_osobnik;
				dopasowanie[osobnik_1] = dopasowanie_syna;
			}
			else
			{
				osobnik[osobnik_2] = nowy_osobnik;
				dopasowanie[osobnik_2] = dopasowanie_syna;
			}

			// Sprawdzamy czy nie odkryliśmy nowego najlepszego osobnika
			if (dopasowanie_syna < najlepsza_sciezka)
			{
				for (int j = 0; j < ilosc_miast; j++)
				{
					najlepszy_osobnik[j] = nowy_osobnik[j];
				}

				najlepsza_sciezka = dopasowanie_syna;
			}
		}

		for (int i = 0; i < populacja; i++)
		{
			if (dopasowanie[i] > najlepsza_sciezka)
			{
				osobnik[i] = mutacjaZamienna(osobnik[i]);
				dopasowanie[i] = obliczDopasowanie(osobnik[i]);
			}
		}

	}
	// Wyświetlenie wyników
	cout << "Koszt: " << najlepsza_sciezka << endl;
	cout << "Sciezka: ";
	for (int i = 0; i < getIloscMiast() - 1; i++)
	{
		cout << najlepszy_osobnik[i] << " -> ";
	}
	cout << najlepszy_osobnik[ilosc_miast - 1] << " -> " << najlepszy_osobnik[0] << endl;


}

//Algorytm Mrówkowy:
int* Komiwojazer::trasa(int* trasa)
{
	int *miasta = new int[getIloscMiast()];
	trasa = new int[getIloscMiast()];
	int start = 0, next = 0;
	start = (rand() % getIloscMiast());   //Losujemy miasto początkowe dla każdej mrówki
	int pomocnicza;
	for (int i = 0; i < getIloscMiast(); i++)
	{
		trasa[i] = -1;
		if (i == start)
		{
			pomocnicza = i;
		}
		miasta[i] = i;
	}
	miasta[start] = -1;
		
	int pomoc = 0;;
	for (int j = 0; j < getIloscMiast(); j++)
	{
		int najwiekszy = 0;
		if (((double)rand() / (RAND_MAX)) < q)
		{
			//Mrówka wybiera droge na której występuję feromon
			for (int i = 0; i < getIloscMiast(); i++)
			{
				if (feromon[start][i] > najwiekszy)
				{
					najwiekszy = feromon[start][i];
					pomoc = i;
				}
			}

			if (miasta[pomoc] >= 0)
			{
				next = pomoc;
				miasta[next] = -1;
			}

			else
			{
				bool pomocnicza = true;
				int pomocnicza2 = 0;
				do {
					next = (rand() % getIloscMiast());
					for (int i = 0; i < getIloscMiast(); i++)
					{
						if (miasta[i] == next)
						{
							miasta[i] = -1;
							pomocnicza = false;
						}

					}
					for (int i = 0; i < getIloscMiast(); i++)
					{
						if (miasta[i] == -1)
						{
							pomocnicza2++;
						}

					}
					if (pomocnicza2 == getIloscMiast())
						break;

				} while (pomocnicza);
			}


			trasa[j] = start;
			start = next;


		}
		else
		{
			//Mrówka wybiera droge losową, tzn. eksploruję przestrzeń
			trasa[j] = start;
			bool pomocnicza = true;
			int pomocnicza2 = 0;
			do {
				next = (rand() % getIloscMiast());
				for (int i = 0; i < getIloscMiast(); i++)
				{
					if (miasta[i] == next)
					{
						miasta[i] = -1;
						pomocnicza = false;
					}

				}
				for (int i = 0; i < getIloscMiast(); i++)
				{
					if (miasta[i] == -1)
					{
						pomocnicza2++;
					}

				}
				if (pomocnicza2 == getIloscMiast())
					break;

			} while (pomocnicza);
			start = next;
		}
	}
	delete miasta;
	return trasa;
}
void Komiwojazer::aktualizacjaFeromonu(int* mrowka)
{
	for (int i = 0; i < getIloscMiast() - 1; i++)
	{
		feromon[mrowka[i]][mrowka[i + 1]]++;
	}
	feromon[mrowka[getIloscMiast() - 1]][mrowka[0]]++;
}
void Komiwojazer::algorytmMrówkowy(int iloscMrowek)
{
	cout << "                ALGORYTM MROWKOWY:              " << endl;
	if (getIloscMiast() < 2) // === WARUNEK ROZPOCZĘCIA ALGORYTMU === //
	{
		cout << "Nie da sie uruchomic algorytmu dla instancji mniejszej od 2!" << endl;
		return;
	}
	int najlepsza_sciezka = INT_MAX;
	feromon = new int*[getIloscMiast()];
	czas = new int*[getIloscMiast()];
	for (int i = 0; i < getIloscMiast(); i++)
	{
		feromon[i] = new int[getIloscMiast()];
		czas[i] = new int[getIloscMiast()];
	}
	for (int i = 0; i < getIloscMiast(); i++)
	{
		for (int j = 0; j < getIloscMiast(); j++)
		{
			feromon[i][j] = 0;
			czas[i][j] = 0;
		}
	}
	mrowki = iloscMrowek;
	int *trasaM = new int[getIloscMiast()];
	int *najlepszaTrasa = new int[getIloscMiast()];
	int odleglosc;
	for (int i = 0; i < mrowki; i++)
	{
		trasaM = trasa(trasaM);
		odleglosc = obliczDopasowanie(trasaM);
		if (odleglosc < najlepsza_sciezka)
		{
			najlepsza_sciezka = odleglosc;
			najlepszaTrasa = trasaM;
			aktualizacjaFeromonu(trasaM);			//Następuje zaktualizowanie feromonu na trasie
		}
	}

	// Wyświetlenie wyników
	cout << "Koszt: " << najlepsza_sciezka << endl;
	cout << "Sciezka: ";
	for (int i = 0; i < getIloscMiast() - 1; i++)
	{
		cout << najlepszaTrasa[i] << " -> ";
	}
	cout << najlepszaTrasa[ilosc_miast - 1] << " -> " << najlepszaTrasa[0] << endl;

}