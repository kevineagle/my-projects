package Paczka;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class User extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	boolean logon = false;

	String url = "jdbc:mysql://localhost:3306";
	String dbuser = "root";
	String dbpassword = "";
	String auser = null;
	private JLabel ls;
	private JLabel ttl;
	
	private JLabel dir;
	private JTextField title;
	private JTextField director;
	private JButton filter;
	private JButton back2;
	private JButton add;
	private JButton confirm;
	private JButton remove;
	private JButton res;
	private DBCon con;
	private ResultSet result;
	Login windowe;
	GUI window;
	static User frame;
	static User framese;
	public void setWindowse(Login asd, GUI cos, String user) {
		windowe = asd;
		window = cos;
		auser = user;

	}

	ArrayList<Movie> movies;

	private static Object[][] moviesTable;
	public static int countmovies;
	private final static Object[] columns = { "Title", "Director", "Date", "Genre", "Type", "Status" };
	DefaultTableModel model = new DefaultTableModel();
	private JTable table;

	private String[] cond = new String[5];
	private JComboBox<String> comboLanguage;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new User();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @throws SQLException
	 */
	public User() throws SQLException {
		super("Logged as User");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING).addGap(0, 774, Short.MAX_VALUE));
		gl_contentPane.setVerticalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING).addGap(0, 551, Short.MAX_VALUE));
		contentPane.setLayout(gl_contentPane);

		movies = new ArrayList<Movie>();
		con = new DBCon(url, dbuser, dbpassword);

		ls = new JLabel(" ");
		ls.setFont(new Font("Tahoma", Font.BOLD, 14));
		ls.setLocation(5, 5);
		ls.setSize(160, 25);
		getContentPane().add(ls);

		ttl = new JLabel("Title:");
		ttl.setFont(new Font("Tahoma", Font.BOLD, 14));
		ttl.setLocation(5, 35);
		ttl.setSize(80, 25);
		getContentPane().add(ttl);
		ttl.setVisible(true);

		dir = new JLabel("Director:");
		dir.setFont(new Font("Tahoma", Font.BOLD, 14));
		dir.setLocation(130, 35);
		dir.setSize(80, 25);
		getContentPane().add(dir);
		dir.setVisible(true);

		title = new JTextField();
		title.setSize(120, 25);
		title.setLocation(5, 65);
		getContentPane().add(title);
		title.setVisible(true);

		director = new JTextField();
		director.setSize(120, 25);
		director.setLocation(130, 65);
		getContentPane().add(director);
		director.setVisible(true);

		result = con.getMovies();
		while (result.next()) {
			movies.add(new Movie(result.getString("title"), result.getString("director"), result.getInt("date"),
					result.getString("type"), result.getString("genre"), result.getString("status")));
		}

		moviesTable = new String[movies.size()][6];
		model.setDataVector(moviesTable, columns);
		countmovies = movies.size();
		for (int i = 0; i < movies.size(); i++) {
			moviesTable[i][0] = movies.get(i).title;
			moviesTable[i][1] = movies.get(i).director;
			moviesTable[i][2] = Integer.toString(movies.get(i).date);
			moviesTable[i][3] = movies.get(i).genre;
			moviesTable[i][4] = movies.get(i).type;
			moviesTable[i][5] = movies.get(i).status;
		}

		table = new JTable(moviesTable, columns);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setLocation(5, 95);
		scrollPane.setSize(575, 400);
		getContentPane().add(scrollPane);
		table.setFillsViewportHeight(true);

		filter = new JButton("Filter");
		filter.setFont(new Font("Tahoma", Font.BOLD, 14));
		filter.setSize(80, 25);
		filter.setLocation(425, 65);
		getContentPane().add(filter);
		filter.setVisible(true);

		back2 = new JButton("Logout");
		back2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				dispose();
				if (windowe != null) {
					windowe.setVisible(true);
					windowe.setWindows(window);

				}
			}
		});
		back2.setFont(new Font("Tahoma", Font.BOLD, 14));
		back2.setSize(150, 25);
		back2.setLocation(625, 35);
		getContentPane().add(back2);
		back2.setVisible(true);
		
		confirm = new JButton("Confirm You Reservation");
		confirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Confirm conf = null;
				try {

					framese = new User();
					conf = new Confirm(auser);
					conf.setWindowse(framese, windowe, window, auser);
					

				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				conf.setVisible(true);
				dispose();
			}
		});
		confirm.setFont(new Font("Tahoma", Font.BOLD, 14));
		confirm.setSize(250, 25);
		confirm.setLocation(250, 495);
		getContentPane().add(confirm);
		confirm.setVisible(true);

		add = new JButton("Add Movie");
		add.setFont(new Font("Tahoma", Font.BOLD, 14));
		add.setSize(123, 25);
		add.setLocation(455, 495);

		remove = new JButton("Remove Movie");
		remove.setFont(new Font("Tahoma", Font.BOLD, 14));
		remove.setSize(123, 25);
		remove.setLocation(455, 525);

		res = new JButton("Make reservation");
		res.setFont(new Font("Tahoma", Font.BOLD, 14));
		res.setSize(180, 25);
		res.setLocation(5, 495);

		FilterHandler filterHandler = new FilterHandler();
		filter.addActionListener(filterHandler);
		filter.setVisible(true);

		ResHandler resHandler = new ResHandler();
		res.addActionListener(resHandler);
		getContentPane().add(res);
		res.setVisible(true);
		
		// define items in a String array:
		String[] languages = new String[] {"-", "Sci-fi", "Musical", "Horror", "Drama", "Criminal","Comedy","Animated","Action"};
		 
	
		
		comboLanguage = new JComboBox<String>(languages);
		comboLanguage.setFont(new Font("Tahoma", Font.BOLD, 14));
		comboLanguage.setSize(80, 25);
		comboLanguage.setLocation(300, 65);
		getContentPane().add(comboLanguage);
		comboLanguage.setVisible(true);

	}

	private class FilterHandler implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			table.setModel(new DefaultTableModel(null, columns));
			moviesTable = new String[movies.size()][6];
			model.setDataVector(moviesTable, columns);
			table.setModel(model);
			if (title.getText().trim().isEmpty())
				cond[0] = null;
			else {
				cond[0] = title.getText();
				result = con.searchMoviesTitle(cond[0]);
			}
			if (director.getText().trim().isEmpty())
				cond[1] = null;
			else {
				cond[1] = director.getText();
				result = con.searchMoviesDirector(cond[1]);
			}
			if (title.getText().trim().isEmpty() & director.getText().trim().isEmpty()) {
				result = con.getMovies();
			}
			movies = new ArrayList<Movie>();
			try {
				while (result.next()) {
					movies.add(new Movie(result.getString("title"), result.getString("director"), result.getInt("date"),
							result.getString("type"), result.getString("genre"), result.getString("status")));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			for (int i = 0; i < movies.size(); i++) {

				table.setValueAt(movies.get(i).title, i, 0);
				table.setValueAt(movies.get(i).director, i, 1);
				table.setValueAt(Integer.toString(movies.get(i).date), i, 2);
				table.setValueAt(movies.get(i).genre, i, 3);
				table.setValueAt(movies.get(i).type, i, 4);
				table.setValueAt(movies.get(i).status, i, 5);

			}
		}
	}

	private class ResHandler implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (movies.size() > 0) {
				String user = auser;
				String movie = (String) table.getValueAt(table.getSelectedRow(), 0);
				int a = 0;
				//System.out.println(user);
				a = con.makeReservation(user, movie);
				if (a == 1) {
					ls.setText(" ");
					ls.setText("Reservated");
				} else {
					ls.setText(" ");
					ls.setText("You can't Reservated");
				}
			} else {
				JOptionPane.showMessageDialog(null, "Movie Base is Empty!!");
			}

		}
	}

}
