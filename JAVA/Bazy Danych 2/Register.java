package Paczka;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.EmptyBorder;

public class Register extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	String url = "jdbc:mysql://localhost:3306";
	String dbuser = "root";
	String dbpassword = "";
	String auser = null;
	private JLabel l;
	private JLabel p;
	private JLabel ls;
	private JLabel eme;
	private JTextField em;
	private JTextField un;
	private JTextField pw;
	private JButton login;
	private JButton back;
	boolean registron = false;
	GUI window;

	public void setWindows(GUI cos) {
		window = cos;

	}

	private DBCon con;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Register frame = new Register();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Register() {
		super("Registry");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING).addGap(0, 774, Short.MAX_VALUE));
		gl_contentPane.setVerticalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING).addGap(0, 552, Short.MAX_VALUE));
		contentPane.setLayout(gl_contentPane);
		con = new DBCon(url, dbuser, dbpassword);

		l = new JLabel("Username: ");
		l.setFont(new Font("Tahoma", Font.BOLD, 14));
		l.setLocation(215, 175);
		l.setSize(80, 25);
		getContentPane().add(l);

		p = new JLabel("Password: ");
		p.setFont(new Font("Tahoma", Font.BOLD, 14));
		p.setLocation(215, 215);
		p.setSize(80, 25);
		getContentPane().add(p);

		ls = new JLabel("dss");
		ls.setFont(new Font("Tahoma", Font.BOLD, 14));
		ls.setLocation(5, 5);
		ls.setSize(160, 25);

		un = new JTextField();
		un.setSize(120, 25);
		un.setLocation(305, 175);
		getContentPane().add(un);

		pw = new JTextField();
		pw.setSize(120, 25);
		pw.setLocation(305, 215);
		getContentPane().add(pw);

		eme = new JLabel("Email: ");
		eme.setFont(new Font("Tahoma", Font.BOLD, 14));
		eme.setLocation(245, 255);
		eme.setSize(80, 25);
		getContentPane().add(eme);

		em = new JTextField();
		em.setSize(120, 25);
		em.setLocation(305, 255);
		getContentPane().add(em);

		login = new JButton("Registry");
		login.setFont(new Font("Tahoma", Font.BOLD, 14));
		login.setSize(120, 25);
		login.setLocation(305, 295);
		getContentPane().add(login);

		back = new JButton("Back");
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				window.setVisible(true);
			}
		});
		back.setFont(new Font("Tahoma", Font.BOLD, 14));
		back.setSize(120, 25);
		back.setLocation(305, 335);
		getContentPane().add(back);

		RegisterHandler registerhandler = new RegisterHandler();
		login.addActionListener(registerhandler);

	}

	private class RegisterHandler implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			String user = un.getText();
			String pass = pw.getText();
			String email = em.getText();

			con.getUser(user, pass, email);

		}
	}

}
