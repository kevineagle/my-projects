package Paczka;

public class Movie {

	String title;
	String director;
	int date;
	String type;
	String genre;
	String status;

	public Movie(String title, String director, int date, String type, String genre, String status) {
		this.title = title;
		this.director = director;
		this.date = date;
		this.type = type;
		this.genre = genre;
		this.status = status;
	}
	
}